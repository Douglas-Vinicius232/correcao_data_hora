#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <ctype.h>

#define MAX_LINE_LENGTH 64
char new_line[MAX_LINE_LENGTH] = {0};

//estrutura de data e hora formato de segundo e microsegundo
struct timeval tv;
//estrutura de data e hora formato de calendario
struct tm tm = {0};
//variavel para registrar a quantidade de horas levando em considerecao a correcao de data e hora
int qtd_horas = 0;
//variavel para registrar o tempo total adiantado em segundos
long tempo_adiantado = 0;
//variavel para registrar o tempo da ultima hora
time_t time_unix_incial = 0;
long multiple = 1;


int fix_date_time(char *data)
{
	struct tm t = {0};
		
	//copia a celula caso nao tenha numero
	if(!(*data >= 0x30 || *data <= 0x39)){
		printf("nenhum numero detectado\n");
		return 0;
	}
	else{
		t.tm_year = strtol(&data[0], NULL, 10) - 1900;
		t.tm_mon = strtol(&data[5], NULL, 10) - 1;
		t.tm_mday = strtol(&data[8], NULL, 10);
		t.tm_hour = strtol(&data[11], NULL, 10);
		t.tm_min = strtol(&data[14], NULL, 10);
		t.tm_sec = strtol(&data[17], NULL, 10);
		
		time_t epoch = mktime(&t);

		//mostra a data e hora da tabela
		printf("mktime %ld,%d-%d-%d %d:%d:%d ",epoch, t.tm_year+1900, t.tm_mon+1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec);
		//caso o valor de timeunix seja invalida retornar falha
		if(epoch<0){
			return 0;
		}else{
			//retira o tempo adiantado acumulado
			epoch -= tempo_adiantado;
			//se nao tiver nenhum valor anterior
			if(time_unix_incial == 0){
					time_unix_incial = epoch;
					memcpy(&tm, localtime(&epoch), sizeof (struct tm));
					//printf("%d-%d-%d %d:%d:%d\n", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
			}
			//comparacao para verificar se passou uma hora
			else if(epoch - time_unix_incial >= multiple*3600)
			{	
				//acresenta o tempo que deve ser retirado
				tempo_adiantado += 24;
				//caso o tempo acumulado passar de uma hora a varivel multiple deve ser acresentada para manter a comparacao de passagem de hora valida
				if((tempo_adiantado/multiple) > 3600)
					multiple ++;
				//acrescenta a variavel que passou mais uma hora
				qtd_horas++;
				printf("passou %d hora(s), atual %ld anterior %ld tempo_adiantado: %lds", qtd_horas, epoch, time_unix_incial, tempo_adiantado);
				//retira o tempo de adiantamento estimado por hora
				epoch -= 24;
				//atribui o valor da ultima hora
				time_unix_incial = epoch;
				//copia a data e hora corrigida para o estrura para ser inserida na tabela
				memcpy(&tm, localtime(&epoch), sizeof (struct tm));
				
			
			}
			//copia a data e hora corrigida para o estrura para ser inserida na tabela
			else{
				memcpy(&tm, localtime(&epoch), sizeof (struct tm));
				}
  		
			return 1;
			}
		}

}
int line_analysis(char *str) 
{
	//arquivo separa as colunas por ','
	char token[2] = {',', '\0'};
	char *pt;
	
	//mac
	pt = strtok(str, token);
	if(pt != NULL){
		strcat(new_line, pt);
	
	}else{return 0;}
	
	//pressao
	pt = strtok(NULL, token);
	if(pt != NULL){
		strcat(new_line, token);
		strcat(new_line, pt);
		pt = NULL;

	}else{return 0;}
	
	//data e hora
	pt = strtok(NULL, token);
	if(pt != NULL)
	{
		char data[20] = {0};
		//copia a data e hora da tabela antes da correcao
		strncpy(data, pt, 20);
		if(fix_date_time(data) == 0){
	
			printf("primeira linha da tabela !!!\n");
			strcat(new_line, token);
			strcat(new_line, pt);
			
		}else {
			char data_formatada[64] = {0};
			strftime(data_formatada, 64, "%Y-%m-%d %H:%M:%S", &tm);
			//sprint(data_formatada, "%d-%d-%d %d:%d:%d", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
			printf(" data_formatada %s \n", data_formatada);
			strcat(new_line, token);
			strcat(new_line, data_formatada);
			pt = NULL;
			}
		
	
	}else{return 0;}
	
	//sub id
	pt = strtok(NULL, token);
	if(pt != NULL){
		strcat(new_line, token);
		strcat(new_line, pt);
		pt = NULL;
	}else{return 0;}

	return 1; 
}

int main() {
    FILE *inputFile, *outputFile;
    char inputFileName[32], outputFileName[32];
    char line[MAX_LINE_LENGTH];

    // Solicitar o nome do arquivo de entrada
    printf("Informe o nome do arquivo de entrada (.txt): ");
    scanf("%s", inputFileName);

    // Abrir o arquivo de entrada para leitura
    if ((inputFile = fopen(inputFileName, "r")) == NULL) {
        perror("Erro ao abrir o arquivo de entrada");
        return EXIT_FAILURE;
    }

    // Solicitar o nome do arquivo de saída
    printf("Informe o nome do arquivo de saída (.txt): ");
    scanf("%s", outputFileName);

    // Abrir o arquivo de saída para escrita
    if ((outputFile = fopen(outputFileName, "w")) == NULL) {
        perror("Erro ao abrir o arquivo de saída");
        fclose(inputFile);
        return EXIT_FAILURE;
    }

    // Processar cada linha do arquivo de entrada
    while (fgets(line, MAX_LINE_LENGTH, inputFile) != NULL) {
        if (line_analysis(line)) {
            // Gravar a linha no arquivo de saída
            fprintf(outputFile, "%s", new_line);
            memset(new_line, 0x00, MAX_LINE_LENGTH);
			memset(line, 0x00, MAX_LINE_LENGTH);
        }
    }

    // Fechar os arquivos
    fclose(inputFile);
    fclose(outputFile);

    printf("fim \n");

    return EXIT_SUCCESS;
}